#
# docker-compose build && docker-compose push
#
# stage: 1
FROM node:16-alpine as builder
RUN mkdir -p /usr/src/app
ENV PORT 3000

WORKDIR /app

COPY package.json /app
COPY yarn.lock /app
RUN yarn install --production
COPY . /app
ENV PATH /app/node_modules/.bin:$PATH
ARG ENV_NAME
ENV REACT_APP_WEB_AUTHEN_HOST "https://web-dohome-center"$ENV_NAME".dohome.technology"
ENV REACT_APP_API_INTERNAL "https://api-internal"$ENV_NAME".dohome.technology"
RUN yarn build


# Stage 2 - the production environment
FROM nginx:alpine
LABEL maintainer="web-scan"

EXPOSE 3000
CMD [ "yarn", "start" ]

