import { useTranslation } from "next-i18next";
import { billingAddressAtom, shippingAddressAtom } from "src/contexts/checkout";
import dynamic from "next/dynamic";

import { AddressType } from "src/utils/constants";
import Seo from "src/components/seo/seo";
import { useUser } from "src/framework/basic-rest/user";

export default function CheckoutPage() {
  const { t } = useTranslation();
  const { me } = useUser();
  // const { id, address, profile } = me ?? {};
  return (
    <>
      <Seo noindex={true} nofollow={true} />
      <div className="px-4 py-8 bg-gray-100 lg:py-10 lg:px-8 xl:py-14 xl:px-16 2xl:px-20">
        <div className="flex flex-col items-center w-full max-w-5xl m-auto rtl:space-x-reverse lg:flex-row lg:items-start lg:space-x-8">
          <div className="w-full space-y-6 lg:max-w-2xl"></div>
          <div className="w-full mt-10 mb-10 sm:mb-12 lg:mb-0 lg:w-96"></div>
        </div>
      </div>
    </>
  );
}
CheckoutPage.authenticationRequired = true;
