import Layout from "@components/layout/layout";
import AccountLayout from "@components/my-account/account-layout";
import WishlistTable from "@components/my-account/wishlist-table";
import { GetStaticProps } from "next";
import { serverSideTranslations } from "next-i18next/serverSideTranslations";

export default function OrdersTablePage() {
	return (
		<AccountLayout>
			<WishlistTable />
		</AccountLayout>
	);
}

OrdersTablePage.Layout = Layout;

export const getStaticProps: GetStaticProps = async ({ locale }) => {
	return {
		props: {
			...(await serverSideTranslations(locale!, [
				"common",
				"forms",
				"menu",
				"footer",
			])),
		},
	};
};
