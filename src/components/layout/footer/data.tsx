
export const footer = {
	widgets: [
		{
			id: 1,
			widgetTitle: "",
			lists: []
		},
		{
			id: 2,
			widgetTitle: "",
			lists: []
		},
		{
			id: 3,
			widgetTitle: "",
			lists: []
		},
		{
			id: 4,
			widgetTitle: "",
			lists: []
		},
		{
			id: 5,
			widgetTitle: "",
			lists: []
		},
		{
			id: 6,
			widgetTitle: "",
			lists: [
				{
					id: 1,
					title: "วันทำการ จันทร์-อาทิตย์",
					path: "",
				},
				{
					id: 2,
					title: "เวลา 9.00-19.00",
					path: "",
				},
				{
					id: 3,
					title: "ติดต่อฝ่ายขาย",
					path: "/contact",
				},
				
			],
		},
	],
	payment: [
		{
			id: 1,
			path: "/",
			image: "/assets/images/payment/mastercard.svg",
			name: "payment-master-card",
			width: 34,
			height: 20,
		},
		{
			id: 2,
			path: "/",
			image: "/assets/images/payment/visa.svg",
			name: "payment-visa",
			width: 50,
			height: 20,
		},
		{
			id: 3,
			path: "/",
			image: "/assets/images/payment/paypal.svg",
			name: "payment-paypal",
			width: 76,
			height: 20,
		},
		{
			id: 4,
			path: "/",
			image: "/assets/images/payment/jcb.svg",
			name: "payment-jcb",
			width: 26,
			height: 20,
		},
		{
			id: 5,
			path: "/",
			image: "/assets/images/payment/skrill.svg",
			name: "payment-skrill",
			width: 39,
			height: 20,
		},
	],
};
