import dynamic from "next/dynamic";
import Modal from "src/components/ui/modal/modal";
import { useModalAction, useModalState } from "./modal.context";

const Login = dynamic(() => import("src/components/auth/login-form"), {
  ssr: false,
});
const Register = dynamic(() => import("src/components/auth/sign-up-form"));
const ForgotPassword = dynamic(
  () => import("src/components/auth/forget-password-form")
);
const ProductDetailsModalView = dynamic(
  () => import("src/components/product/product-popup"),
  { ssr: false }
);

const CreateOrUpdateAddressForm = dynamic(
  () => import("src/components/address/address-form"),
  { ssr: false }
);

const AddressDeleteView = dynamic(
  () => import("src/components/address/delete-view")
);

const ManagedModal = () => {
  const { isOpen, view } = useModalState();
  const { closeModal } = useModalAction();

  return (
    <Modal open={isOpen} onClose={closeModal}>
      {view === "LOGIN_VIEW" && <Login />}
      {view === "REGISTER" && <Register />}
      {view === "FORGOT_VIEW" && <ForgotPassword />}

      {view === "ADD_OR_UPDATE_ADDRESS" && <CreateOrUpdateAddressForm />}

      {view === "DELETE_ADDRESS" && <AddressDeleteView />}
      {view === "PRODUCT_DETAILS" && <ProductDetailsModalView />}
    </Modal>
  );
};

export default ManagedModal;
