import cn from "classnames";
import Image from "next/image";
import { FC, useState } from "react";
import { useUI } from "@contexts/ui.context";
import usePrice from "@framework/product/use-price";
import { Product } from "@framework/types";
import router from "next/router";
import { ROUTES } from "@utils/routes";
import { useCart } from "@contexts/cart/cart.context";
import isEmpty from "lodash/isEmpty";
import Counter from "@components/common/countersmall";
import { getVariations } from "@framework/utils/get-variations";
import { generateCartItem } from "@utils/generate-cart-item";
import Button from "@components/ui/button";
import {IoHeart,IoCart } from "react-icons/io5";
import { useTranslation } from "next-i18next";
import ListBox from "@components/ui/smalllistbox";
interface ProductProps {
	product: Product;
	className?: string;
	contactClassName?: string;
	imageContentClassName?: string;
	variant?: "grid" | "gridSlim" | "list" | "listSmall";
	imgWidth?: number | string;
	imgHeight?: number | string;
	imgLoading?: "eager" | "lazy";
}

const ProductCard: FC<ProductProps> = ({
	product,
	className = "",
	contactClassName = "",
	imageContentClassName = "",
	variant = "list",
	imgWidth = 340,
	imgHeight = 440,
	imgLoading,
}) => {
	const { t } = useTranslation("common");
	const { addItemToCart } = useCart();
	const variations = getVariations(product.variations);
	console.log(variations, "variations");
	const [quantity, setQuantity] = useState(1);
	const [attributes, setAttributes] = useState<{ [key: string]: string }>({});
	const [viewCartBtn, setViewCartBtn] = useState<boolean>(false);
	const [addToCartLoader, setAddToCartLoader] = useState<boolean>(false);
	const { openModal, setModalView, setModalData } = useUI();
	const placeholderImage = `/assets/placeholder/products/product-${variant}.svg`;
	const { price, basePrice, discount } = usePrice({
		amount: product.sale_price ? product.sale_price : product.price,
		baseAmount: product.price,
		currencyCode: "THB",
	});

	function handlePopupView() {
		setModalData({ data: product });
		setModalView("PRODUCT_VIEW");
		return openModal();
	}
	function navigateToProductPage() {
		router.push(`${ROUTES.PRODUCT}/${product.slug}`, undefined, {
			locale: router.locale,
		});
	}

	const isSelected = !isEmpty(variations)
		? !isEmpty(attributes) &&
		  Object.keys(variations).every((variation) =>
				attributes.hasOwnProperty(variation)
		  )
		: true;

	function addToCart() {
		if (!isSelected) return;
		// to show btn feedback while product carting
		setAddToCartLoader(true);
		setTimeout(() => {
			setAddToCartLoader(false);
			setViewCartBtn(true);
		}, 600);
		const item = generateCartItem(product!, attributes);
		addItemToCart(item, quantity);
		console.log(item, "item");
	}


	return (
		<div
			className={cn(
				"group box-border overflow-hidden flex rounded-md cursor-pointer",
				{
					"pe-0 pb-2 lg:pb-3 flex-col items-start bg-white transition duration-200 ease-in-out transform hover:-translate-y-1 hover:md:-translate-y-1.5 hover:shadow-product":
						variant === "grid",
					"pe-0 md:pb-1 flex-col items-start bg-white": variant === "gridSlim",
					"items-center bg-transparent border border-gray-100 transition duration-200 ease-in-out transform hover:-translate-y-1 hover:shadow-listProduct":
						variant === "listSmall",
					"flex-row items-center transition-transform ease-linear bg-gray-200 pe-2 lg:pe-3 2xl:pe-4":
						variant === "list",
				},
				className
			)}
	
			role="button"
			title={product?.name}
		>
			<div
				className={cn(
					"flex",
					{
						"mb-3 md:mb-3.5": variant === "grid",
						"mb-3 md:mb-3.5 pb-0": variant === "gridSlim",
						"flex-shrink-0 w-32 sm:w-44 md:w-36 lg:w-44":
							variant === "listSmall",
					},
					imageContentClassName
				)}
			>
				<Image
					src={product?.image?.thumbnail ?? placeholderImage}
					width={imgWidth}
					height={imgHeight}
					loading={imgLoading}
					quality={100}
					alt={product?.name || "Product Image"}
					onClick={navigateToProductPage}
					className={cn("bg-gray-300 object-cover rounded-s-md", {
						"w-full rounded-md transition duration-200 ease-in group-hover:rounded-b-none":
							variant === "grid",
						"rounded-md transition duration-150 ease-linear transform group-hover:scale-105":
							variant === "gridSlim",
						"rounded-s-md transition duration-200 ease-linear transform group-hover:scale-105":
							variant === "list",
					})}
				/>
			</div>
			<div
				className={cn(
					"w-full overflow-hidden",
					{
						"ps-0 lg:ps-2.5 xl:ps-4 pe-2.5 xl:pe-4": variant === "grid",
						"ps-0": variant === "gridSlim",
						"px-4 lg:px-5 2xl:px-4": variant === "listSmall",
					},
					contactClassName
				)}
			>
				<h2
					className={cn("text-heading font-semibold truncate mb-1", {
						"text-sm md:text-base": variant === "grid",
						"md:mb-1.5 text-sm sm:text-base md:text-sm lg:text-base xl:text-lg":
							variant === "gridSlim",
						"text-sm sm:text-base md:mb-1.5 pb-0": variant === "listSmall",
						"text-sm sm:text-base md:text-sm lg:text-base xl:text-lg md:mb-1.5":
							variant === "list",
					})}
				>
					{product?.name}
				</h2>
				<p className="text-body text-xs lg:text-sm leading-normal xl:leading-relaxed max-w-[250px] truncate">
					รหัสสินค้า:	{product?.id}
				</p>
				{product?.description && (
					<p className="text-body text-xs lg:text-sm leading-normal xl:leading-relaxed max-w-[250px] truncate">
						{product?.description}
					</p>
				)}
					<div
					className={`text-heading font-semibold text-sm sm:text-base mt-1.5 space-s-2 ${
						variant === "grid"
							? "lg:text-lg lg:mt-2.5"
							: "sm:text-xl md:text-base lg:text-xl md:mt-2.5 2xl:mt-3"
					}`}
				>
					<span className="inline-block sm:text-base font-normal text-red">{price}</span>
					{discount && (
						<del className="sm:text-base font-normal text-gray-800">
							{basePrice}
						</del>
					)}
				</div>
						<div className="pt-2 md:pt-4">
						<div className="flex items-center justify-between mb-4 space-s-3 sm:space-s-4">
						<ListBox
							options={[
						{ name: "ชิ้น", value: "item" },
						{ name: "กล่อง", value: "box" },

						]}
							/>
							<Counter
								quantity={quantity}
								onIncrement={() => setQuantity((prev) => prev + 1)}
								onDecrement={() =>
									setQuantity((prev) => (prev !== 1 ? prev - 1 : 1))
								}
								disableDecrement={quantity === 1}
							/>
							
						</div>
						<div className="flex items-center justify-between mb-4 space-s-3 sm:space-s-4">
						<Button
								onClick={addToCart}
								variant="flat"
								className={`w-full h-4 md:h-4 px-1.5 ${
									!isSelected && "bg-orange-400 hover:bg-orange-400"
								}`}
								disabled={!isSelected}
								loading={addToCartLoader}
							>
								  <IoHeart/>
							</Button>
							<Button
								onClick={addToCart}
								variant="flat"
								className={`w-full h-4 md:h-4 px-1.5 ${
									!isSelected && "bg-orange-400 hover:bg-orange-400"
								}`}
								disabled={!isSelected}
								loading={addToCartLoader}
							>
								<IoCart/>
							</Button>
						</div>
					</div>
			
			</div>
		</div>
	);
};

export default ProductCard;
