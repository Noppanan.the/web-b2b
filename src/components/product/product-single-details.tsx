import React, { useState } from 'react';
import Button from '@components/ui/button';
import Counter from '@components/common/counter';
import { useRouter } from 'next/router';
import { useProductQuery } from '@framework/product/get-product';
import { getVariations } from '@framework/utils/get-variations';
import usePrice from '@framework/product/use-price';
import { useCart } from '@contexts/cart/cart.context';
import { generateCartItem } from '@utils/generate-cart-item';
import { ProductAttributes } from './product-attributes';
import isEmpty from 'lodash/isEmpty';
import Link from '@components/ui/link';
import { toast } from 'react-toastify';
import { useWindowSize } from '@utils/use-window-size';
import Carousel from '@components/ui/carousel/carousel';
import { Swiper, SwiperSlide } from 'swiper/react';
import ProductMetaReview from '@components/product/product-meta-review';
import ListBox from '@components/ui/smalllistbox';
import { Thumbs } from 'swiper';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import { Carousel as Carou } from 'react-responsive-carousel';
const productGalleryCarouselResponsive = {
  '768': {
    slidesPerView: 2,
  },
  '0': {
    slidesPerView: 1,
  },
};

const ProductSingleDetails: React.FC = () => {
  const {
    query: { slug },
  } = useRouter();
  const { width } = useWindowSize();
  const { data, isLoading } = useProductQuery(slug as string);
  const { addItemToCart } = useCart();
  const [attributes, setAttributes] = useState<{ [key: string]: string }>({});
  const [quantity, setQuantity] = useState(1);
  const [addToCartLoader, setAddToCartLoader] = useState<boolean>(false);
  const [thumbsSwiper, setThumbsSwiper] = useState(null);
  const { price, basePrice, discount } = usePrice(
    data && {
      amount: data.sale_price ? data.sale_price : data.price,
      baseAmount: data.price,
      currencyCode: 'THB',
    },
  );
  if (isLoading) return <p>Loading...</p>;
  const variations = getVariations(data?.variations);

  const isSelected = !isEmpty(variations)
    ? !isEmpty(attributes) && Object.keys(variations).every((variation) => attributes.hasOwnProperty(variation))
    : true;

  function addToCart() {
    if (!isSelected) return;
    // to show btn feedback while product carting
    setAddToCartLoader(true);
    setTimeout(() => {
      setAddToCartLoader(false);
    }, 600);

    const item = generateCartItem(data!, attributes);
    addItemToCart(item, quantity);
    toast('เพิ่มสินค้าลงตะกร้า', {
      type: 'dark',
      progressClassName: 'fancy-progress-bar',
      position: width > 768 ? 'bottom-right' : 'top-right',
      autoClose: 2000,
      hideProgressBar: false,
      closeOnClick: true,
      pauseOnHover: true,
      draggable: true,
    });
    console.log(item, 'item');
  }

  function handleAttribute(attribute: any) {
    setAttributes((prev) => ({
      ...prev,
      ...attribute,
    }));
  }

  return (
    <div className="block lg:grid grid-cols-12 gap-x-10 xl:gap-x-14 pt-7 pb-10 lg:pb-14 2xl:pb-20 items-start">
      <div className="col-span-4 grid grid-cols-1 gap-2.5">
        <Carou>
          {data?.gallery?.map((item, index: number) => (
            <div key={index} className="col-span-1 transition duration-150 ease-in hover:opacity-90">
              <img
                src={item?.original ?? '/assets/placeholder/products/product-gallery.svg'}
                alt={`${data?.name}--${index}`}
                className="object-cover w-full"
              />
            </div>
          ))}
        </Carou>
      </div>
      {/* 
			{width < 1025 ? (
				<Carousel
					pagination={{
						clickable: true,
					}}
					breakpoints={productGalleryCarouselResponsive}
					className="product-gallery"
					buttonClassName="hidden"
				>
					{data?.gallery?.map((item, index: number) => (
						<SwiperSlide key={`product-gallery-key-${index}`}>
							<div className="col-span-1 transition duration-150 ease-in hover:opacity-90">
								<img
									src={
										item?.original ??
										"/assets/placeholder/products/product-gallery.svg"
									}
									alt={`${data?.name}--${index}`}
									className="object-cover w-full"
								/>
							</div>
						</SwiperSlide>
					))}
				</Carousel>
			) : (
				<div className="col-span-5 grid grid-cols-2 gap-2.5">
					{data?.gallery?.map((item, index: number) => (
						<div
							key={index}
							className="col-span-1 transition duration-150 ease-in hover:opacity-90"
						>
							<img
								src={
									item?.original ??
									"/assets/placeholder/products/product-gallery.svg"
								}
								alt={`${data?.name}--${index}`}
								className="object-cover w-full"
							/>
						</div>
					))}
				</div>
			)}
 */}
      <div className="col-span-4 grid grid-cols-1 gap-x-10">
        <div className="pb-7 mb-7  border-gray-300">
          <h2 className="text-heading text-lg md:text-xl lg:text-2xl 2xl:text-3xl font-bold hover:text-black mb-3.5">
            {data?.name}
          </h2>

          <div className="flex items-center">
            <div className="text-body font-bold text-base md:text-xl lg:text-2xl 2xl:text-4xl pe-2 md:pe-0 lg:pe-2 2xl:pe-0">
            ราคาส่ง  {price}
            </div>
            {discount && (
              <span className="line-through font-segoe text-gray-400 text-sm md:text-base lg:text-lg xl:text-xl ps-2">
                {basePrice}
              </span>
            )}
          </div>
		  <div className="text-body font-bold text-base ">
			ราคาต่อหน่วยย่อย {data?.price} บาท
		  </div>
        </div>

        <div>
          <div className="flex items-center mt-5">
		  <div className="text-body font-bold text-base ">
            หน่วยนับสินค้า 
			</div>
    
          </div>
	
		  <div className="col-span-3 pt-8 lg:pt-5 " style={{width:"220px"}}>
              <ListBox
			 
                options={[
                  { name: 'ชิ้น', value: 'item' },
                  { name: 'กล่อง', value: 'box' },
                ]}
              />
			  </div>
       
          <table className="w-full text-heading  text-sm lg:text-base pt-8  lg:pt-5">
            <thead>
              <tr>
                <th className="bg-gray-150 p-3 text-start first:rounded-ts-md w-1/5 text-center">จำนวน</th>
                <th className="bg-gray-150 p-3 text-start  w-1/5 text-center">ราคา</th>
                <th className="bg-gray-150 p-3 text-start  w-1/5 text-center">ส่วนลด</th>
                <th className="bg-gray-150 p-3 text-start  w-1/5 text-center">ราคาสุทธิ</th>
                <th className="bg-gray-150 p-3 text-start last:rounded-te-md w-1/5 text-center ">ต่อหน่วย</th>
              </tr>
            </thead>
            <tbody>
              <tr className="odd:bg-white">
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">1</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">780.00</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">0</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">780.00</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">780.00</td>
              </tr>
              <tr className="odd:bg-white">
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">5</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">3900.00</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">2</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">3822.00</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">764.40</td>
              </tr>
              <tr className="odd:bg-white">
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">10</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">7800.00</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">3</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">7566.00</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">756.60</td>
              </tr>
              <tr className="odd:bg-white">
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">50</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">39000.00</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">4</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">37440.00</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">748.80</td>
              </tr>
              <tr className="odd:bg-white">
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">100</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">78000.00</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">5</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">74100.00</td>
                <td className="text-sm text-gray-900 font-light px-6 py-4 whitespace-nowrap">741.00</td>
              </tr>
            </tbody>
            <tfoot></tfoot>
          </table>
		  <p className="text-body text-sm lg:text-base leading-6 lg:leading-8">{data?.description}</p>
        </div>
        <table className="w-full text-heading font-semibold text-sm lg:text-base">ราคาแนะนำขาย 790 บาท/ชิ้น</table>

        {/* 
        <div className="py-6">
          <ul className="text-sm space-y-5 pb-1">
            <li>
              <span className="font-semibold text-heading inline-block pe-2">SKU:</span>
              {data?.sku}
            </li>
            <li>
              <span className="font-semibold text-heading inline-block pe-2">Category:</span>
              <Link href="/" className="transition hover:underline hover:text-heading">
                {data?.category?.name}
              </Link>
            </li>
            {data?.tags && Array.isArray(data.tags) && (
              <li className="productTags">
                <span className="font-semibold text-heading inline-block pe-2">Tags:</span>
                {data.tags.map((tag) => (
                  <Link
                    key={tag.id}
                    href={tag.slug}
                    className="inline-block pe-1.5 transition hover:underline hover:text-heading last:pe-0"
                  >
                    {tag.name}
                    <span className="text-heading">,</span>
                  </Link>
                ))}
              </li>
            )}
          </ul>
        </div> */}
      </div>
      <div className="col-span-4 grid grid-cols-1 gap-x-10">
        <div className="pb-3 ">
          {Object.keys(variations).map((variation) => {
            return (
              <ProductAttributes
                key={variation}
                title={variation}
                attributes={variations[variation]}
                active={attributes[variation]}
                onClick={handleAttribute}
              />
            );
          })}
        </div>
		<div className="pb-3 ">
			ราคารวมภาษีมูลค่าเพิ่ม
		
			</div>
			<div className="pb-3 ">
			<p className="text-body text-sm lg:text-base leading-6 lg:leading-8 text-red">
				
			{data?.sale_price * quantity}
			</p>
				</div>
        <div className="flex items-center space-s-4 md:pe-32 lg:pe-12 2xl:pe-32 3xl:pe-48 border-b border-gray-300 py-8">
          <Counter
            quantity={quantity}
            onIncrement={() => setQuantity((prev) => prev + 1)}
            onDecrement={() => setQuantity((prev) => (prev !== 1 ? prev - 1 : 1))}
            disableDecrement={quantity === 1}
          />
          <Button
            onClick={addToCart}
            variant="slim"
            className={`w-full md:w-6/12 xl:w-full ${!isSelected && 'bg-gray-400 hover:bg-gray-400'}`}
            disabled={!isSelected}
            loading={addToCartLoader}
          >
            <span className="py-2 3xl:px-8">Add</span>
          </Button>
        </div>
      </div>
	  <div className="col-span-8 grid grid-cols-1 gap-2.5">	<ProductMetaReview data={data} /></div>
    </div>

	
  );
};

export default ProductSingleDetails;
