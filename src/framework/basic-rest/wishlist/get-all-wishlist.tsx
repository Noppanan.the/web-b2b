import { QueryOptionsType, Wishlist } from "@framework/types";
import http from "@framework/utils/http";
import { API_ENDPOINTS } from "@framework/utils/api-endpoints";
import { useQuery } from "react-query";

export const fetchWish = async ({ queryKey }: any) => {
  const [_key, _params] = queryKey;
  const {
    data: { data },
  } = await http.get(API_ENDPOINTS.WISHLIST);
  return { wish: { data: data as Wishlist[] } };
};
export const useOrdersQuery = (options: QueryOptionsType) => {
  return useQuery<{ wish: { data: Wishlist[] } }, Error>(
    [API_ENDPOINTS.WISHLIST_TYPE, options],
    fetchWish
  );
};
