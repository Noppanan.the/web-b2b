import { useUI } from "@contexts/ui.context";
import { API_ENDPOINTS } from "@framework/utils/api-endpoints";
import http from "@framework/utils/http";
import Cookies from "js-cookie";
import { useMutation, useQuery, useQueryClient } from "react-query";
import { useAtom } from "jotai";
import { authorizationAtom } from "src/framework/basic-rest/utils/get-token";
import { toast } from "react-toastify";
export interface SignUpInputType {
  email: string;
  password: string;
  name: string;
}
export interface SocialLoginInputType {
  provider: string;
  access_token: string;
}
async function signUp(input: SignUpInputType) {
  return http.post(API_ENDPOINTS.LOGIN, input);
  // return {
  //   token: `${input.email}.${input.name}`.split("").reverse().join(""),
  // };
}
async function socialLogin(input: SocialLoginInputType) {
  return http.post(API_ENDPOINTS.SOCIAL_LOGIN, input);
}
export function useUser(input: SignUpInputType) {
  const [isAuthorized] = useAtom(authorizationAtom);
  const { data, isLoading, error } = useQuery([API_ENDPOINTS.LOGIN, input], {
    enabled: isAuthorized,
    onError: (err) => {
      console.log(err);
    },
  });
  //TODO: do some improvement here
  return { me: data, isLoading, error, isAuthorized };
}

export const useSignUpMutation = () => {
  const { authorize, closeModal } = useUI();
  return useMutation((input: SignUpInputType) => signUp(input), {
    onSuccess: (data) => {
      Cookies.set("auth_token", data?.data?.token);
      authorize();
      closeModal();
    },
    onError: (data) => {
      console.log(data, "login error response");
    },
  });
};

export function useSocialLogin() {
  const queryClient = useQueryClient();

  const [_, setAuthorized] = useAtom(authorizationAtom);

  return useMutation((input: SocialLoginInputType) => socialLogin(input), {
    onSuccess: (data) => {
      if (data?.data.token && data?.data.permissions?.length) {
        Cookies.set("auth_token", data.data.token);
        setAuthorized(true);
        return;
      }
      if (!data.data.token) {
        toast.error("Something went wrong");
      }
    },
    onError: (data) => {
      toast.error("Something went wrong " + data, { autoClose: false });
    },
    onSettled: () => {
      queryClient.clear();
    },
  });
}
