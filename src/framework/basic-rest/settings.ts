import type { Settings } from "src/types";
import { useMutation, useQuery } from "react-query";
import client from "./client";
import { API_ENDPOINTS } from "@framework/utils/api-endpoints";
import { useState } from "react";
import { FileWithPath } from "react-dropzone";
import { getPreviewImage } from "src/utils/get-preview-image";
import { useAtom } from "jotai";
import { couponAtom } from "src/contexts/checkout";
import { toast } from "react-toastify";
import { useTranslation } from "next-i18next";

export function useSettings() {
  const { data, isLoading, error } = useQuery<Settings, Error>(
    [API_ENDPOINTS.SETTINGS],
    client.settings.all
  );
  return {
    settings: data?.options ?? {},
    isLoading,
    error,
  };
}

export const useUploads = ({ onChange, defaultFiles }: any) => {
  const [files, setFiles] = useState<FileWithPath[]>(
    getPreviewImage(defaultFiles)
  );

  const { mutate: upload, isLoading } = useMutation(client.settings.upload, {
    onSuccess: (data: any) => {
      if (onChange) {
        const dataAfterRemoveTypename = data?.map(
          ({ __typename, ...rest }: any) => rest
        );
        onChange(dataAfterRemoveTypename);
        setFiles(getPreviewImage(dataAfterRemoveTypename));
      }
    },
  });

  function handleSubmit(data: File[]) {
    upload(data as any);
  }

  return { mutate: handleSubmit, isLoading, files };
};

export function useSubscription() {
  let [isSubscribed, setIsSubscribed] = useState(false);

  const subscription = useMutation(client.users.subscribe, {
    onSuccess: () => {
      setIsSubscribed(true);
    },
    onError: () => {
      setIsSubscribed(false);
    },
  });

  return {
    ...subscription,
    isSubscribed,
  };
}

export function useVerifyCoupon() {
  const { t } = useTranslation();
  const [_, applyCoupon] = useAtom(couponAtom);
  let [formError, setFormError] = useState<any>(null);

  const { mutate, isLoading } = useMutation(client.coupons.verify, {
    onSuccess: (data) => {
      if (!(data as any).is_valid) {
        setFormError({
          code: t("error-invalid-coupon"),
        });
      }

      applyCoupon((data as any).coupon);
    },
    onError: (error) => {
      const {
        response: { data },
      }: any = error ?? {};

      toast.error(data?.message);
    },
  });

  return { mutate, isLoading, formError, setFormError };
}
