import axios from 'axios';
import { getAuthCredentials} from "./auth-utils";
//import { getEnv } from 'src/config/get-env';
const { oauth} = getAuthCredentials();
const request = axios.create({
  baseURL: process.env.NEXT_PUBLIC_REST_API_ENDPOINT,
  //baseURL: getEnv('REACT_APP_WEB_AUTHEN_HOST'), // TODO: take this api URL from env
  timeout: 30000,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization:   `Bearer ${oauth?.access_token}`,
  },
});

// Change request data/error here
request.interceptors.request.use(
  (config) => {
    
    config.headers = {
      ...config.headers,
      Authorization: `Bearer ${oauth?.access_token ? oauth?.access_token : ''}`,
    };
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

export default request;
