import Cookie from "js-cookie";
import SSRCookie from "cookie";
import {
  AUTH_CRED,
  // PERMISSIONS,
  STAFF,
  STORE_OWNER,
  SUPER_ADMIN,
  TOKEN,
  oauth,
  user_info
} from "./constants";
import jwtDecode from 'jwt-decode';
import Cookies from "js-cookie";
export const allowedRoles = [SUPER_ADMIN, STORE_OWNER, STAFF];
export const adminAndOwnerOnly = [SUPER_ADMIN, STORE_OWNER];
export const adminOwnerAndStaffOnly = [SUPER_ADMIN, STORE_OWNER, STAFF];
export const adminOnly = [SUPER_ADMIN];
export const ownerOnly = [STORE_OWNER];
export const oauth2 = {oauth};
export const userinfo = {user_info};
export function setAuthCredentials(token: string, permissions: any,oauth: any,userinfo: any) {

  Cookie.set(AUTH_CRED, JSON.stringify({ token, permissions ,oauth,userinfo }));

}

export function getAuthCredentials(context?: any): {
  token: string | null;
  permissions: string[] | null;
  oauth: {
    login_uuid: string | null,
    access_uuid: string | null,
    access_token: string | null,
    at_expires: 0,
    refresh_uuid: string | null,
    refresh_token: string | null,
    rt_expires: 0,
  } | null;
  userinfo: {
    userId: string | null,
    moduleId: string | null,
    rolesActoins: {},
    rolesCode: [],
    actionsCode: [],
    menusCode: [],
  } | null;
} 
{
  let authCred;
  if (context) {
    authCred = parseSSRCookie(context)[AUTH_CRED];
  } else {
    authCred = Cookies.get(AUTH_CRED);
    
  }

  if (authCred) {
 
    return JSON.parse(authCred);
  }
  return { token: null, permissions: null,oauth :{
    login_uuid: '',
    access_uuid: '',
    access_token: '',
    at_expires: 0,
    refresh_uuid: '',
    refresh_token: '',
    rt_expires: 0,
  } ,userinfo:{
    userId: '',
    moduleId: '',
    rolesActoins: {},
    rolesCode: [],
    actionsCode: [],
    menusCode: [],
  }};
}

export function parseSSRCookie(context: any) {
  return SSRCookie.parse(context.req.headers.cookie ?? "");
}
export function isValidToken(token?: string | null) {
  if (!token) {
    return false;
  }
  const decoded: any = jwtDecode(token);
  const currentTime = Date.now() / 1000;
  return decoded.exp > currentTime;
};
export function hasAccess(
  _allowedRoles: string[],
//  _userPermissions: string[] | undefined | null
) {
  if (_allowedRoles) {
    return true
  }
  return false;
}
export function isAuthenticated(_cookies: any) {
  return (
    !!_cookies[TOKEN] 
  );
}



