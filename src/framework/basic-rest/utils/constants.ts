export const CUSTOMER = "customer";
export const LIMIT = 10;
export const SUPER_ADMIN = "super_admin";
export const STORE_OWNER = "store_owner";
export const STAFF = "staff";
export const TOKEN = "token";
export const PERMISSIONS = "permissions";
export const AUTH_CRED = "AUTH_CRED";
export const user_info =   {
    "userId": "",
    "userName": "",
    "uuid": "",
    "employeeLevel": "",
    "positionName": "",
    "actionTime": "",
    "ip": "",
    "passwordExpired": true
}
export const oauth = {
        "login_uuid": "",
        "access_uuid": "",
        "access_token": "",
        "at_expires": 0,
        "refresh_uuid": "",
        "refresh_token": "",
        "rt_expires": 0
}