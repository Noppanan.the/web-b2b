import { atom } from "jotai";
import Cookies from "js-cookie";

export const getToken = () => {
  
  if (typeof window === undefined) {
    return null;
  }

  return Cookies.get("auth_token");
};

export function checkIsLoggedIn() {
  const token = Cookies.get('auth_token');
  if (!token) return false;
  return true;
}
export const authorizationAtom = atom(checkIsLoggedIn());


export function useToken() {
  return {
    setToken(token: string) {
      Cookies.set('auth_token', token, { expires: 1 });
    },
    getToken() {
      return Cookies.get('auth_token');
    },
    removeToken() {
      Cookies.remove('auth_token');
    },
    hasToken() {
      const token = Cookies.get('auth_token');
      if (!token) return false;
      return true;
    },
  };
}
