export const API_ENDPOINTS = {
  LOGIN: "/login",
  SOCIAL_LOGIN: "login",
  REGISTER: "/register",
  LOGOUT: "/logout",
  FORGET_PASSWORD: "/forget-password",
  CATEGORIES: "/categories.json",
  TYPE: "/type.json",
  FEATURED_CATEGORIES: "/featured_categories.json",
  COLLECTIONS: "/collections.json",
  BRANDS: "/brands.json",
  PRODUCTS: "/products.json",
  FEATURED_PRODUCTS: "/featured_products.json",
  TOP_SELLER_PRODUCTS: "/products_top_seller.json",
  ON_SELLING_PRODUCTS: "/products_on_selling.json",
  PRODUCT: "/product.json",
  RELATED_PRODUCTS: "/related_products.json",
  BEST_SELLER_PRODUCTS: "/products_best_seller.json",
  NEW_ARRIVAL_PRODUCTS: "/products_new_arrival.json",
  FLASH_SALE_PRODUCTS: "/products_flash_sale.json",
  SEARCH: "/search.json",
  ORDERS: "/orders.json",
  ORDER: "/order.json",
  WISHLIST: "/wishlist.json",
  WISHLIST_TYPE: "/wishlist_type.json",

  PRODUCTS_POPULAR: "/popular-products",

  TYPES: "/types",
  TAGS: "/tags",
  SHOPS: "/shops",
  AUTHORS: "/authors",
  AUTHORS_TOP: "/top-authors",
  MANUFACTURERS: "/manufacturers",
  MANUFACTURERS_TOP: "/top-manufacturers",
  COUPONS: "/coupons",
  COUPONS_VERIFY: "/coupons/verify",

  ORDERS_STATUS: "/order-status",
  ORDERS_REFUNDS: "/refunds",
  ORDERS_CHECKOUT_VERIFY: "/orders/checkout/verify",
  ORDERS_DOWNLOADS: "/downloads",
  GENERATE_DOWNLOADABLE_PRODUCT_LINK: "/downloads/digital_file",
  USERS: "/users",
  USERS_ADDRESS: "/address",
  USERS_ME: "/me",
  USERS_LOGIN: "/token",
  USERS_REGISTER: "/register",
  USERS_FORGOT_PASSWORD: "/forget-password",
  USERS_VERIFY_FORGOT_PASSWORD_TOKEN: "/verify-forget-password-token",
  USERS_RESET_PASSWORD: "/reset-password",
  USERS_CHANGE_PASSWORD: "/change-password",
  USERS_LOGOUT: "/logout",
  USERS_SUBSCRIBE_TO_NEWSLETTER: "/subscribe-to-newsletter",
  USERS_CONTACT_US: "/contact-us",

  SEND_OTP_CODE: "send-otp-code",
  VERIFY_OTP_CODE: "verify-otp-code",
  OTP_LOGIN: "otp-login",
  UPDATE_CONTACT: "update-contact",
  SETTINGS: "/settings",
  UPLOADS: "/attachments",
};
