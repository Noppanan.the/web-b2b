import { ProductsQueryType, Type } from "@framework/types";
import http from "@framework/utils/http";
import { API_ENDPOINTS } from "@framework/utils/api-endpoints";
import { useQuery } from "react-query";

export const fetchType = async ({ queryKey }: any) => {
	const [_key, _params] = queryKey;
	const {
		data: { data },
	} = await http.get(API_ENDPOINTS.TYPE);
	return { types: { data: data as Type[] } };
};
export const useCategoriesQuery = (options: ProductsQueryType) => {
	return useQuery<{ categories: { data: Type[] } }, Error>(
		[API_ENDPOINTS.TYPE, options],
      //  fetchType
	);
};
